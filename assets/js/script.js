
// =============== Vue instance ==================
var vm = new Vue({
  el:'#app',
  data:{
    CurrentDate :new Date(),
    CurrentTimeStamp: Date.now(),
		CurrentDateYear: new Date().getFullYear(),
		CurrentDateMonth: new Date().getMonth()+1,
		CurrentDateDay: new Date().getDate(),
		userDate: '',
		userFormat:'',
		days:30,
  },
  methods:{


	CurrentJalaliDate:function(date,xxx){
		return getCurrentJalaliDate();
	},

	persianNumber: function(amount){
		amount = amount + '';
		// add , to amount
		amount = amount.replace(/0/g, '۰');
		amount = amount.replace(/1/g, '۱');
		amount = amount.replace(/2/g, '۲');
		amount = amount.replace(/3/g, '۳');
		amount = amount.replace(/4/g, '۴');
		amount = amount.replace(/5/g, '۵');
		amount = amount.replace(/6/g, '۶');
		amount = amount.replace(/7/g, '۷');
		amount = amount.replace(/8/g, '۸');
		amount = amount.replace(/9/g, '۹');

		return amount

	},
  },
});
