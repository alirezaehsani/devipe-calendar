//================================
function getCurrentGregorianDate(){
	let currentDate = new Date();
	let year = currentDate.getFullYear();
	let month = currentDate.getMonth();
	let day = currentDate.getDate();
	let gregorianDate = [year,month+1,day];
	return gregorianDate;


}

//================================
function getCurrentJalaliDate(){
	let currentDate = new Date();
	let gregorianYear = currentDate.getFullYear();
	let gregorianMonth = currentDate.getMonth();
	let gregorianDay = currentDate.getDate();
	let jalaliDate = gregorian_to_jalali(gregorianYear,gregorianMonth+1,gregorianDay);
	jalaliDate = [jalaliDate[0],jalaliDate[1],jalaliDate[2]];
	return jalaliDate;
}

//================================
function getGregorianFromTimestamp(timestamp){
	let gregorianDate = new Date(timestamp);
	return gregorianDate;
}

//================================
function getPreviousMonth(year,month){
	if (month == 1){
		month = 12;
		year -= 1;
	}

	else{
		month -= 1;
	}

	let prevMonth = new Date(year,month-1,1);
	return prevMonth;
}

//==================================
function getNextMonth(year,month){
	if (month == 12){
		month = 1;
		year += 1;
	}
	let nextMonth = new Date(year,month-1,1);
	return nextMonth;
}

//================================
function gregorianDaysInMonth(year,month){
	let date = new Date(year,month-1,0);
	return date.getDate();
}

//================================
function jalaliDaysInMonth(year,month){
	let isLeapYear = jalaliLeapYear(year);
	if (month <= 6){
		return 31;
	}
	else if (month == 12 && !isLeapYear){
		return 29;
	}
	else if (month == 12 && isLeapYear){
		return 30;
	}
	else{
		return 30;
	}
}

//================================
function jalaliLeapYear(jalaliYear){
	let remainders = [30,26,22,17,13,9,5,1];
	let divideRemainder = jalaliYear % 33;
	if (remainders.includes(divideRemainder)){
		return true;
	}
	else{
		return false;
	}
}

//================================
function jalaliDayIndex(year,month,day){
	jalaliDaysindex = [1,2,3,4,5,6,0];
	// convert jalali date to gregorian
	let gregorianDate = jalali_to_gregorian(year,month,day);
	let gregorianYear = gregorianDate[0];
	let gregorianMonth = gregorianDate[1];
	let gregorianDay = gregorianDate[2];
	gregorianDate = new Date(gregorianYear,gregorianMonth-1,gregorianDay);
	let gregorianDayIndex = gregorianDate.getDay();

	let jalaliDayIndex = jalaliDaysindex[gregorianDayIndex];

	return jalaliDayIndex;
}

//================================
function gregorian_to_jalali(gregorianYear , gregorianMonth , gregorianDay){

		var gregorianDate = new Date(gregorianYear,gregorianMonth-1,gregorianDay)

		// Number of passed days in the beginning of each gregorian month:
        var g_d_m=[0,31,59,90,120,151,181,212,243,273,304,334];

		// Calculating jalali year:
        var jalaliYear = (gregorianYear <= 1600) ? 0 : 979;
        gregorianYear -= (gregorianYear <= 1600) ? 621 : 1600;
        var gregorianYear2 = (gregorianMonth > 2) ? (gregorianYear + 1) : gregorianYear;

		// Calculating days:
        var days = (365*gregorianYear) +(parseInt((gregorianYear2+3)/4)) -(parseInt((gregorianYear2+99)/100))
                +(parseInt((gregorianYear2+399)/400)) -80 +gregorianDay +g_d_m[gregorianMonth-1];

        jalaliYear += 33 * (parseInt(days / 12053));
        days %= 12053;
        jalaliYear += 4 * (parseInt(days/1461));
        days %= 1461;
        jalaliYear += parseInt((days - 1) / 365);
        if (days > 365) days= (days - 1) % 365;

		// Calculating jalali month:
        var jalaliMonth = (days < 186) ? (1 + parseInt(days / 31)) : 7 + parseInt((days - 186) / 30) ;

		// Calculating jalali Day:
        var jalaliDay = 1 + ((days < 186) ? (days % 31) : ((days - 186) %30));


		let jalaliDate = [jalaliYear,jalaliMonth,jalaliDay]
        return jalaliDate;
}


//================================
function jalali_to_gregorian(jalaliYear , jalaliMonth , jalaliDay){

		// Calculating gregorian year:
    var gregorianYear = (jalaliYear <= 979) ? 621 : 1600;
    jalaliYear -= (jalaliYear <= 979) ? 0 : 979;

		// Calculating gregorian days:
    var days=(365 * jalaliYear) +((parseInt(jalaliYear / 33)) * 8) +(parseInt(((jalaliYear % 33) + 3) / 4))
              +78 +jalaliDay +((jalaliMonth < 7) ? (jalaliMonth - 1) * 31 : ((jalaliMonth - 7) * 30) + 186);
    gregorianYear += 400 * (parseInt(days / 146097));
      days%=146097;
    if(days > 36524){
    	gregorianYear += 100 * (parseInt(--days / 36524));
      days%=36524;
      if(days >= 365)days++;
    }

  	gregorianYear += 4 * (parseInt((days) / 1461));
    days%=1461;
  	gregorianYear += parseInt((days - 1) / 365);
    if(days > 365)days=(days-1)%365;
    var gregorianDay = days+1;
    var sal_a=[0,31,((gregorianYear % 4==0 && gregorianYear%100!=0) || (gregorianYear%400==0))?29:28,31,30,31,30,31,31,30,31,30,31];
    var gregorianMonth
    for(gregorianMonth=0;gregorianMonth<13;gregorianMonth++){
      var v=sal_a[gregorianMonth];
      if(gregorianDay <= v)break;
      gregorianDay-=v;
    }

		var gregorianDate = new Date(gregorianYear,gregorianMonth-1,gregorianDay)
		// Catch the name of day:



        return [gregorianYear,gregorianMonth,gregorianDay];
    }

//========================================

function format(date,dateFormat){
	// get Day according to user Date format
	function formatDate(date,format){
		let year = date[0];
		let month = date[1];
		let dayNum = date[2];
		date = new Date(year+'-'+month+'-'+dayNum);

		switch (format){
			default:
				return 'Invalid Date Format';

			case 'D': // displays day number
				return dayNum;

			case 'DD': // displays day number with two digits
				dayLength = dayNum.toString().length;
				if (dayLength == 2){
					return dayNum;
				}
				else{
					return '0'+dayNum;
				}

			case 'DDD': // displays day of the year
				var now = date;
				var start = new Date(year, 0, 0);
				var diff = now - start;
				var oneDay = 1000 * 60 * 60 * 24;
				var day = Math.floor(diff / oneDay);
				return + day;

			case 'DN': // day number of week
			return jalaliDayIndex(year,month,dayNum)+1;

			case 'JDC': // jalali month days
				// NOTE: date should be jalali Date
				if (month <= 6){
					return 31;
				}
				else{
					return 30;
				}

			case 'GD':// displays gregorian day name
				dayNumOfWeek = date.getDay();
				gregorianDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
				return gregorianDays[dayNumOfWeek];

			case 'JD'://displays jalali day name
				dayIndex = jalaliDayIndex(year,month,dayNum);
				var jalaliDays = ['شنبه','یکشنبه','دوشنبه','سه شنبه','چهارشنبه','پنجشنبه'
,'جمعه',];
				return jalaliDays[dayIndex];

			case 'M': // display the number of month
				return month;

			case 'MM': //display the number of month with two digits
				monthLength = month.toString().length;
				if (monthLength == 2){
					return month;
				}
				else{
					return '0'+(month);
				}

			case 'JMfa': // displays jalali month name (persian)
				// NOTE: date should be jalali Date
				jalaliMonths = ['فروردین','اردیبهشت','خرداد','تیر','مرداد','شهریور','مهر','آبان','آذر','دی','بهمن','اسفند',];
				return jalaliMonths[month-1];

			case 'JMen': // display jalali month name (english)
				// NOTE: date should be jalali Date
				jalaliMonths = ['Farvardin','Ordibehesht','Khordad','Tir','Mordad','Shahrivar','Mehr','Aban','Azar','Dey','Bahman','Esfand'];
				return jalaliMonths[month-1];

			case 'GMfa': // displays gregorian month name (persian)
				// NOTE: date should be gregorian Date
				gregorianMonths = ['ژانویه','فوریه','مارس','آوریل','می','جون','جولای','آگوست','سپتامبر','اکتبر','نوامبر','دسامبر',];
				return gregorianMonths[month-1];

			case 'GMen':// displays gregorian month name (english)
				// NOTE: date should be gregorian Date
				gregorianMonths = ['January','February','March','April','May','June','July','August','September','October','November','December'];
				return gregorianMonths[month-1];

			case 'YY'://displays year number with two digits
				strYear = year.toString();
				return strYear[2]+strYear[3];

			case 'YYYY'://displays full year number (four digits)
				return year;

			case 'W': //displays week of year
    			date = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
				// Make Sunday's day number 7
				date.setUTCDate(date.getUTCDate() + 4 - (date.getUTCDay()||7));
				// Get first day of year
				var yearStart = new Date(Date.UTC(date.getUTCFullYear(),0,1));
				// Calculate full weeks to nearest Thursday
				var weekNo = Math.ceil(( ( (date - yearStart) / 86400000) + 1)/7);
				// Return array of year and week number
				return weekNo;

		}
	}
	//==========================

	let seprators = ['.','-','/',','];
	let seprator = '';
	for (index in dateFormat){
		if (seprators.includes(dateFormat[index])){
			seprator = dateFormat[index];
		}
	}

	let formatedDate = '';
	if (seprator != ''){
		dateFormat = dateFormat.split(seprator);
		for (index in dateFormat){
			let thisFormatedDate = formatDate(date,dateFormat[index]);
			if (index == (dateFormat.length-1)){
				formatedDate += thisFormatedDate;
			}
			else {
				formatedDate += thisFormatedDate + seprator;
			}
		}
	}
	else{// if there is no seprator in date (single field)
		formatedDate = formatDate(date,dateFormat);
	}


	return formatedDate;
}



function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
