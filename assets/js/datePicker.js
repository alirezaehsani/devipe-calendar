
// =============== Vue instance ==================
var vm = new Vue({
  el:'#app',
  data:{
    monthNames : ['فروردین','اردیبهشت','خرداد','تیر','مرداد','شهریور','مهر','آبان','آذر','دی','بهمن','اسفند',],
		userDate: '',
		userFormat:'',
    JalaliDate:'',
    year:'',
    month:'',
    activeDay:'',
    activeDayNumber:'',

    // user date:
    chosenDay:'',
    chosenMonth:'',
    chosenYear:'',
    chosenDate:'',
    userFormat:'YYYY/MM/DD/JD',

    monthName:'',
		days:'',
    prevMonthDays:'',
    nextMonthDays:'',
    selectMonth:false,
    selectYear:false,
    pickDate:true,
    overlay:'open',
  },

  methods:{

    openDatePicker: function(){
      this.pickDate = true;
      this.overlay = 'open';
    },

    closeDatePicker: function(){
      this.pickDate = false;
      this.overlay = 'close';
    },

    persianNumber: function(amount){
    	amount = amount + '';
    	// add , to amount
    	amount = amount.replace(/0/g, '۰');
    	amount = amount.replace(/1/g, '۱');
    	amount = amount.replace(/2/g, '۲');
    	amount = amount.replace(/3/g, '۳');
    	amount = amount.replace(/4/g, '۴');
    	amount = amount.replace(/5/g, '۵');
    	amount = amount.replace(/6/g, '۶');
    	amount = amount.replace(/7/g, '۷');
    	amount = amount.replace(/8/g, '۸');
    	amount = amount.replace(/9/g, '۹');

    	return amount

    },

    buildPage(date){
      this.JalaliDate = format(date,'YYYY/M/D/JMfa').split('/');
      this.year = parseInt(this.JalaliDate[0]);
      this.month = parseInt(this.JalaliDate[1]);
      this.activeDay = parseInt(this.JalaliDate[2]);
      this.monthName = this.JalaliDate[3];
      this.days = jalaliDaysInMonth(this.year,this.month);
      // previous month days
      let prevMonth = getPreviousMonth(this.year,this.month);
      let prevMonthDaysInMonth = jalaliDaysInMonth(prevMonth.getFullYear(),prevMonth.getMonth()+1);

      // index of current month's first day
      let currentMonthFirstDayIndex = jalaliDayIndex(this.year,this.month,1)-1;

      // index of current month's last day
      let currentMonthLastDayIndex = jalaliDayIndex(this.year,this.month,this.days);

      let prevMonthRemainingDays = [];

      for (let i=prevMonthDaysInMonth-currentMonthFirstDayIndex;
           currentMonthFirstDayIndex > -1;
           i++ , currentMonthFirstDayIndex--){
          prevMonthRemainingDays.push(i);
      }
      let nextMonthRemainingDays =  7 - currentMonthLastDayIndex-1;

      this.prevMonthDays = prevMonthRemainingDays;
      this.nextMonthDays = nextMonthRemainingDays;


    },

    nextMonth: async function(){
      document.getElementsByClassName("days")[0].style.opacity = '0';
      await sleep(500);

      if (this.month == 12){
        this.month = 1;
        this.year += 1;
      }
      else{
        this.month +=1;

      }
      this.buildPage([this.year,this.month,this.activeDay]);
      document.getElementsByClassName("days")[0].style.opacity = '1';
      this.selectMonth = false;
      this.selectYear = false;

    },


    previousMonth: async function(){
      document.getElementsByClassName("days")[0].style.opacity = '0';
      await sleep(500);
      if (this.month == 1){
        this.month = 12;
        this.year -= 1;
      }
      else{
        this.month -=1;
      }
      this.buildPage([this.year,this.month,this.activeDay]);
      document.getElementsByClassName("days")[0].style.opacity = '1';
      this.selectMonth = false;
      this.selectYear = false;
    },

    changeDay(day){
      this.activeDay = day;
      this.buildPage([this.year,this.month,this.activeDay]);
    },

    changeMonth(monthIndex){
      this.month = monthIndex + 1;
      this.buildPage([this.year,this.month,this.activeDay]);
      this.selectMonth = false;
      this.selectYear = false;

    },

    changeYear(year){
      this.year = year;
      this.buildPage([this.year,this.month,this.activeDay]);
      this.selectMonth = false;
      this.selectYear = false;
    },

    goToToday(){
      let currentJalaliDate = getCurrentJalaliDate();
      let currentJalaliYear = currentJalaliDate[0];
      let currentJalaliMonth = currentJalaliDate[1];
      let currentJalaliDay = currentJalaliDate[2];
      this.buildPage([currentJalaliYear,currentJalaliMonth,currentJalaliDay]);
    },

    chooseDate(){
      this.chosenDay = this.activeDay;
      this.chosenMonth = this.month;
      this.chosenYear = this.year;
      let chosenDate = [this.chosenYear,this.chosenMonth,this.chosenDay];
      this.chosenDate = format(chosenDate,this.userFormat);
      this.closeDatePicker();
    },
  },

  mounted(){
    this.goToToday();
  }

});
